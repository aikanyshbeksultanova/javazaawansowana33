package main.collectionsss;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ClinicTest {

    @Test
    void shouldRegisterPatient() {
        //given
        Clinic clinic=new Clinic();

        //when
        clinic.registerPatient("Aika");

        //then
        assertThat(clinic.getQuequeSize()).isEqualTo(1);
    }
    @Test
    void shouldHandlePatient() {
        //given
        Clinic clinic=new Clinic();
        clinic.registerPatient("Aika");
        clinic.registerPatient("Michal");
        clinic.registerPatient("Aisha");

        //when
        String result=clinic.handlePatient();

        //then
        assertThat(clinic.getQuequeSize()).isEqualTo(2);
        assertThat(result).isEqualTo("Aika");
    }
    @Test
    void shouldReturnNullWhenQuequeIsEmpty() {
        //given
        Clinic clinic=new Clinic();

        //when
        String result=clinic.handlePatient();

        //then
        assertThat(clinic.getQuequeSize()).isEqualTo(null);
    }
}
