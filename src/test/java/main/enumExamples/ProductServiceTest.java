package main.enumExamples;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ProductServiceTest {
    @Test
    void shouldRetriveFruits() {
        //given
        List<Product> products= Arrays.asList(
                new Product ("Banan",5,1,ProductType.FRUIT),
                new Product ("Monitor",400,5,ProductType.ELECTRONIC),
                new Product ("Orange",6,1,ProductType.FRUIT),
                new Product ("Potato",4,1,ProductType.VEGETABLE)
                );

        //when
        List<Product> result = ProductService.retrieveFruits(products);

        //then
                assertThat(result).hasSize(2);

    }
}
