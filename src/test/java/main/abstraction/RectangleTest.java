package main.abstraction;

import org.assertj.core.api.AbstractBigDecimalAssert;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;

public class RectangleTest {
    @Test
    void shouldCalculateCircuit() {
        //given
        Rectangle rectangle = new Rectangle(5, 2);

        //when
        double result = rectangle.calculateCircuit();

        //then
        assertThat(result).isEqualTo(14);
        //Test z marginesem błędu w wyniku
        assertThat(result).isEqualTo(14, offset(0.1));
    }


    @Test
    void shouldCalculateArea() {
        //given
        Rectangle rectangle = new Rectangle(5, 2);

        //when
        double result = rectangle.calculateArea();

        //then
        assertThat(result).isEqualTo(10);
    }

}
