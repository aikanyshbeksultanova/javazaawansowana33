package main.abstraction;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CircleTest {
    @Test
    void shouldCalculateArea() {
        Circle circle = new Circle(7);

        double result = circle.calculateArea();

        assertThat(result).isEqualTo(153.86, Offset.offset(0.3));

    }

    @Test
    void shouldCalculateCircuit() {
        Circle circle = new Circle (7);

        double result = circle.calculateCircuit();


        assertThat(result).isEqualTo(43.96,Offset.offset(0.3));
    }
}
