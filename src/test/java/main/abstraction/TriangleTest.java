package main.abstraction;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TriangleTest {
    @Test
    void shouldCalculateArea() {
        Triangle triangle = new Triangle(4,6,9);

        double result= triangle.calculateArea();

        assertThat(result).isEqualTo(12, Offset.offset(0.1));
    }

    @Test
    void shiuldCalculateCircuit() {
        Triangle triangle =new Triangle(4,6,9);

        double result = triangle.calculateCircuit();

        assertThat(result).isEqualTo(19,Offset.offset(0.1));

    }
}
