package main.collectionsss;


import java.util.ArrayDeque;
import java.util.Queue;

public class Clinic {
    private Queue<String> patientQueue=new ArrayDeque<>();
  
    public void registerPatient(String patient){
        patientQueue.offer(patient);
      
    }
    public String handlePatient(){
        return patientQueue.poll();
    }
    public int getQuequeSize(){
        return patientQueue.size();

    }
}
