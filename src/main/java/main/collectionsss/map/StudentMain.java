package main.collectionsss.map;

import java.util.*;

public class StudentMain {
    public static void main(String[] args) {
        Student student1=new Student("Michal","Chmelewski","PL");
        List<Integer> grades1= Arrays.asList(4,2,3,2);
        Student student2=new Student("Katarzyna","Wojciechowska","PL");
        List<Integer> grades2= Arrays.asList(5,2,6,2);
        Student student3=new Student("Jurek","Nowak","EN");
        List<Integer> grades3= Arrays.asList(6,3,2,5);
        Student student4=new Student("Adrian","Jerry","DE");
        List<Integer> grades4= Arrays.asList(2,5,4,6);


        Map<Student, List<Integer>> map=new LinkedHashMap<>();
        map.put(student1,grades1);
        map.put(student2,grades2);
        map.put(student3,grades3);
        map.put(student4,grades4);


        for (Map.Entry<Student, List<Integer>> entry : map.entrySet()) {
            System.out.println("Student: " + entry.getKey());
            System.out.println("Oceny: " + entry.getValue());
        }

    }
}
