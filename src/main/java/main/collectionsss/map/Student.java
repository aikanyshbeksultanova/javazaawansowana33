package main.collectionsss.map;


import java.util.Objects;

public class Student {
    private String firstname;
    private String lastname;
    private String mainLanguage;

    public Student(String firstname, String lastname, String mainLanguage) {
        this.firstname = firstname;
        this.lastname=lastname;
        this.mainLanguage=mainLanguage;


    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMainLanguage() {
        return mainLanguage;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMainLanguage(String mainLanguage) {
        this.mainLanguage = mainLanguage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(firstname, student.firstname) && Objects.equals(lastname, student.lastname) && Objects.equals(mainLanguage, student.mainLanguage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, lastname, mainLanguage);
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", mainLanguage='" + mainLanguage + '\'' +
                '}';
    }
}
