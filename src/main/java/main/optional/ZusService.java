package main.optional;

import java.math.BigDecimal;

public interface ZusService {
    BigDecimal applyZus(BigDecimal decimal);

    static ZusService input(){
        return decimal -> decimal.multiply(BigDecimal.valueOf(0.23));
    }
    static ZusService output(){
        return decimal -> decimal.multiply(BigDecimal.valueOf(0.25));
    }

}
