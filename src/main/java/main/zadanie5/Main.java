package main.zadanie5;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.list;

public class Main {

    public static void main(String[] args) {
        List<String> list=Arrays.asList("Andrzej","Andrzej","Wera","Michal");

        list = removeDuplicates(list);

        System.out.println(list);

    }

    public static List<String>removeDuplicates(List<String>list){
        return  list.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
