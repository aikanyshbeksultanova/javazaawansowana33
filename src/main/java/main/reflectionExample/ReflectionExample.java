package main.reflectionExample;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionExample {
    public static void main(String[] args)throws NoSuchMethodException, InvocationTargetException,IllegalAccessException {
        Printer printer=new Printer("Canon","MG3650");
        printer.CanonPrinter();
        for (Method metod:Printer.class.getDeclaredMethods()) {
            System.out.println(metod.getName());
        }
        //Printer printer1=ReflectionExample.class.getDeclaredMethod("LGPrinter");
    }
}
