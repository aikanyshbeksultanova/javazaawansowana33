package main.singleton;

public class CarMain {
    public static void main(String[] args) {
        Car car= new Car.Builder("Toyota","Corolla")
                .withYear(2020)
                .withColor("white")
                .build();

        Car car2 =new Car.Builder("Ford","Focus")
                .withYear(2021)
                .build();
    }
}
