package main.singleton;

public class Car {
    private String brand;
    private String model;
    private int year;
    private String color;



    private Car(){

    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", color='" + color + '\'' +
                '}';
    }

    public static class Builder{
        private String brand;
        private String model;
        private int year;
        private String color;

        public Builder(String brand, String model){
            this.brand=brand;
            this.model=model;
        }
        public Builder withYear(int year){
            this.year=year;
            return this;
        }
        public Builder withColor(String color) {
            this.color = color;
            return this;
        }
        public Car build(){
            Car car=new Car();

            car.brand=this.brand;
            car.model=this.model;
            car.year=this.year;
            car.color=this.color;
            return car;
        }
    }
}
