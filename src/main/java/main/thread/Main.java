package main.thread;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        List<Worker> workers= Arrays.asList(
                new Worker ("Aika",1000),
                new Worker("Michal",2000),
                new Worker("Beata",3000)
        );
        for (Worker worker : workers) {
            new Thread(worker).start();
        }
        Thread.sleep(5_000);

        for (Worker worker : workers) {
            worker.setName("Changed");
        }

        Thread.sleep(10_000);

        for (Worker worker : workers) {
            worker.turnOff();
        }



    }
}
