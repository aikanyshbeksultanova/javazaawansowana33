package main.thread;

public class Worker implements Runnable{
    private String name;
    private int sleepTime;
    private boolean active=true;

    public void turnOff(){
        this.active=false;
    }

    public Worker(String name, int sleepTime) {
        this.name = name;
        this.sleepTime=sleepTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (true){
        try {
            Thread.sleep(sleepTime);
            System.out.println(name+" "+"working...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    }
}
