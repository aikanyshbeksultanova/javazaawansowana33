package main.enumExamples;

import java.util.ArrayList;
import java.util.List;

public class ProductService {
    public static List<Product> retrieveFruits(List<Product> products){
        List<Product>result=new ArrayList<>();

        for (Product product : products) {
            if(product.getProductType() == ProductType.FRUIT){
                result.add(product);
            }

        }
        return result;
    }
}
