package main.enumExamples;

public enum ProductType {
    FRUIT("Fruit",100),
    VEGETABLE("Vegetable",200),
    ELECTRONIC("Electronic",300),
    FLUID("Fluid",400);

    private String name;
    private int code;


    ProductType(String name,int code) {
        this.name = name;
        this.code=code;
    }


    public String getName() {
        return name;
    }

    public int getCode() {
        return code;
    }
}
