package main.kompozycja;

public class Computer {
    private Processor processor;
    private GraphicCard graphicCard;
    private PowerSupply powerSupply;

    public Computer(Processor processor, GraphicCard graphicCard, PowerSupply powerSupply){
        this.processor=processor;
        this.graphicCard=graphicCard;
        this.powerSupply=powerSupply;

    }

    public Processor getProcessor() {
        return processor;
    }

    public GraphicCard getGraphicCard() {
        return graphicCard;
    }

    public PowerSupply getPowerSupply() {
        return powerSupply;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor=" + processor +
                ", graphicCard=" + graphicCard +
                ", powerSupply=" + powerSupply +
                '}';
    }
}
