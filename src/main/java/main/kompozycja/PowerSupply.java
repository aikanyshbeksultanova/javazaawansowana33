package main.kompozycja;

public class PowerSupply {
    private String name;

    public PowerSupply(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PowerSupply{" +
                "name='" + name + '\'' +
                '}';
    }
}
