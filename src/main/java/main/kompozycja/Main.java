package main.kompozycja;


public class Main {
    public static void main(String[] args) {

        Processor processor = new Processor("i3 8130U");
        GraphicCard graphicCard = new GraphicCard("RTX 2070");
        PowerSupply powerSupply = new PowerSupply("Corsair 650W");

        Computer computer = new Computer(processor, graphicCard, powerSupply);

        System.out.println(computer);
//kompozycja trzech komponentow na jednym pole.
        Computer c2=ComputerFactory.creatComputer("i9","RTX2080","650W");
        System.out.println(c2);


    }

}
