package main.dziedzicina;

public class Cat extends Animal {
    public Cat(String name, int age,String race){
        super (name,age,race);
    }

    @Override
    public String toString() {
        return "Cat{}";
    }

    @Override
    public void voice(){
    System.out.println("Miauuu");
}
}
