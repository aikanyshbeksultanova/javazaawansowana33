package main.dziedzicina;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal ("Zwierz",5, "");
        System.out.println(animal);

        Cat cat = new Cat ("Mruczek",3,"dachowiec");
        System.out.println(cat);

        Dog dog = new Dog("Alfa",7,"buldog", "tricolor");
        System.out.println(dog);

        Animal[] animals = {animal, cat ,dog};
        for(Animal a: animals) {
            a.voice();

        }
    }
}
