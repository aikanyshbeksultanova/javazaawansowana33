package main.dziedzicina;

public class Dog extends Animal {
    private String color;
    public Dog (String name, int age,String race, String color){
        super(name,age,race);
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return super.toString() + "color" + color+
                '}';
    }

    @Override
    public void voice() {
        System.out.println("hau hau");
    }
}
