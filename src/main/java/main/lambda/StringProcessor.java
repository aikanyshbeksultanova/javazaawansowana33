package main.lambda;

public class StringProcessor {
private StringOperation stringOperation;

    public StringProcessor(StringOperation stringOperation) {
        this.stringOperation = stringOperation;
    }

    public StringOperation getStringOperation() {
        return stringOperation;
    }

    public void setStringOperation(StringOperation stringOperation) {
        this.stringOperation = stringOperation;
    }

    public String process (String input){
        return stringOperation.operation(input);
    }
}
