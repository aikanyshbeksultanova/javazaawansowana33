package main.lambda;



public class Main {
    public static void main(String[] args) {

        StringProcessor processor = new StringProcessor(word -> word.toUpperCase());
        String result = processor.process("ala Ma kota");
        System.out.println(result);


        processor.setStringOperation(String::toLowerCase);
        result = processor.process("TEKST");
        System.out.println(result);


        processor.setStringOperation(input -> input.substring(0, 3));

        result = processor.process("sobota");

        System.out.println(result);


    }
}
