package main.lambda;
@FunctionalInterface

public interface StringOperation {
    String operation(String input);
}
