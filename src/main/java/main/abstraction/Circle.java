package main.abstraction;

public class Circle extends Figure {
    private double r;

    public Circle(double r){
        this.r=r;

    }

    @Override
    public double calculateArea() {
        return Math.PI*(r*r);
    }

    @Override
    public double calculateCircuit() {
        return 2*Math.PI*r;
    }
}
