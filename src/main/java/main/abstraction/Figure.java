package main.abstraction;

public abstract class Figure {


    public abstract double calculateArea();

    public abstract double calculateCircuit();


}
