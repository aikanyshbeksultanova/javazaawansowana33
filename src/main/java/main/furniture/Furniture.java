package main.furniture;

public class Furniture {
    private int dlugosc;
    private int szerokosc;
    private int wysokosc;

    public Furniture(int dlugosc,int szerokosc, int wysokosc){
        this.dlugosc=dlugosc;
        this.szerokosc= szerokosc;
        this.wysokosc=wysokosc;
    }

    public int getDlugosc() {
        return dlugosc;
    }

    public int getSzerokosc() {
        return szerokosc;
    }

    public int getWysokosc() {
        return wysokosc;
    }

    public void setDlugosc(int dlugosc) {
        this.dlugosc = dlugosc;
    }

    public void setSzerokosc(int szerokosc) {
        this.szerokosc = szerokosc;
    }

    public void setWysokosc(int wysokosc) {
        this.wysokosc = wysokosc;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "dlugosc=" + dlugosc +
                ", szerokosc=" + szerokosc +
                ", wysokosc=" + wysokosc +
                '}';
    }
}
