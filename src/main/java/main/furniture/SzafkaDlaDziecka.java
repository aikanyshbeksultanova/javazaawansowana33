package main.furniture;

public class SzafkaDlaDziecka extends Szafka{
    private String color;
    public SzafkaDlaDziecka(int dlugosc, int szerokosc, int wysokosc){
        super(dlugosc,szerokosc,wysokosc);
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return super.toString() + "color" +color;
    }
}
