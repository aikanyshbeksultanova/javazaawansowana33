package main.exeptionsExample;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class User {
    private String login;
    private String password;
    private String firstname;
    private String email;
    private LocalDateTime creationDate;

    private User(String login, String password, String firstname, String email, LocalDateTime creationDate) {
        this.login=login;
        this.password=password;
        this.firstname=firstname;
        this.email=email;
        this.creationDate= creationDate;
    }



    public static User create(String login, String password, String firstname, String email) {
        //Jesli chociaz jeden z parametrow metody jest nullem, rzuc NPE.
        if (login == null || password == null || firstname == null || email == null) {
            throw new NullPointerException();
        }
        if (login.length() < 6) {
            throw new IllegalArgumentException("Login is too short!");
        }

        //sprawdzic czy login nie jest za krotki i rzucic wyjatek

        //stworzyc obiekt User
        return new User(login, password, firstname, email, LocalDateTime.now());
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", email='" + email + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
