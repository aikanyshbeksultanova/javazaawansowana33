package main.exeptionsExample;

public class Main {
    public static void main(String[] args) {
        try {
            User user = User.create("login213213", "admin1234",
                    "Adam", "admin@gmail.com");
            System.out.println(user);
        } catch (NullPointerException e) {
            System.out.println("Nie udalo sie stworzyc uzytkownika.");
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid argument. " + e.getMessage());

        }finally {
            System.out.println("Koniec.");
        }
    }
}
