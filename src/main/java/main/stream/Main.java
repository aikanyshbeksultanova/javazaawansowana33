package main.stream;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Room> list = Arrays.asList(
                new Room("Lublin", 50, 400000, 1),
                new Room("Warszawa", 45, 600000, 1),
                new Room("Katowice", 65, 450000, 1),
                new Room("Bialystok", 75, 380000, 1),
                new Room("Lublin", 35, 320000, 1)
        );

        AuctionHouse auctionHouse = new AuctionHouse(list);

        List<Room> result = auctionHouse.retrieveRoomsFromCity("Lublin");

        System.out.println(result);

    }
}
