package main.stream;

public class Room {
    private String city;
    private double m2;
    private double price;
    private double distanceFromCentre;

    public Room(String city, double m2, double price,double distancefromcentre) {
        this.city = city;
        this.m2=m2;
        this.price=price;
        this.distanceFromCentre=distancefromcentre;
    }

    public String getCity() {
        return city;
    }

    public double getM2() {
        return m2;
    }

    public double getPrice() {
        return price;
    }

    public double getDistanceFromcentre() {
        return distanceFromCentre;
    }

    @Override
    public String toString() {
        return "Room{" +
                "city='" + city + '\'' +
                ", m2=" + m2 +
                ", price=" + price +
                ", distanceFromCentre=" + distanceFromCentre +
                '}';
    }
}
