package main.stream;

import java.util.List;
import java.util.stream.Collectors;

public class AuctionHouse {
    private List<Room> rooms;
    public AuctionHouse(List<Room>rooms){
        this.rooms=rooms;
    }
   public List<Room>retrieveRoomsFromCity(String city){
        return rooms.stream()
                .filter(room->room.getCity().equals(city))
                .collect(Collectors.toList());
   }
   public List<Room>retrieveRoomsBasedOnArea(double min, double max){
        return null;
   }
   public List<Double> retrievePrices(){
        return rooms.stream()
                .map(Room::getPrice)
                .collect(Collectors.toList());
   }
   public List<Room>retrieveClosestRooms(int quantity){
        return null;
   }
}
